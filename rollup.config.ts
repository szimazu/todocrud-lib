import { nodeResolve } from '@rollup/plugin-node-resolve'
import typescript from 'rollup-plugin-typescript2'
import image from '@rollup/plugin-image'
import postcss from 'rollup-plugin-postcss'
import commonjs from '@rollup/plugin-commonjs'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import { babel } from '@rollup/plugin-babel'
import autoExternal from 'rollup-plugin-auto-external'
import { terser } from 'rollup-plugin-terser'
import filesize from 'rollup-plugin-filesize'
import json from '@rollup/plugin-json'

export default [
  {
    input: './src/index.ts',
    output: [
      {
        file: 'build/index.es.js',
        format: 'es',
        exports: 'named',
      },
    ],
    external: [
      '@babel/runtime',
      'bootstrap',
      'react-bootstrap',
      'react-hook-form',
      'react-phone-input-2',
      'react-intl',
    ],
    plugins: [
      commonjs(),
      nodeResolve(),
      peerDepsExternal(),
      autoExternal(),
      image(),
      postcss({
        extensions: ['.css'],
        minimize: true,
      }),
      babel({
        babelHelpers: 'runtime',
        plugins: ['@babel/plugin-transform-runtime'],
      }),
      typescript({ useTsconfigDeclarationDir: true }),
      filesize(),
      terser(),
      json(),
    ],
  },
]
