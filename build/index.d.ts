import 'bootstrap/dist/css/bootstrap.min.css';
import { InputForm } from './Components/InputForm/InputForm';
import { Btn } from './Components/Btn/Btn';
import { TasksList } from './Components/TasksList/TasksList';
import { FormInputType, TaskItemType } from './Components/types';
export { InputForm, Btn, TasksList, FormInputType, TaskItemType };
