import React from 'react';
export interface BtnProps {
    variant?: 'primary' | 'danger' | 'success';
    onClick: () => void;
    disabled?: boolean;
}
export declare function Btn({ variant, disabled, onClick, children, }: React.PropsWithChildren<BtnProps>): JSX.Element;
