/// <reference types="react" />
import { TaskItemType, Lang } from '../types';
import './TasksList.css';
export interface TasksListProps {
    todoItems: TaskItemType[];
    completeTask: (id: string) => void;
    deteteTask: (id: string) => void;
    lang?: Lang;
}
export declare function TasksList({ todoItems, completeTask, deteteTask, lang, }: TasksListProps): JSX.Element;
