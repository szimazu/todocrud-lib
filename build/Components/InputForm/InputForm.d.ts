/// <reference types="react" />
import 'react-phone-input-2/lib/style.css';
import { FormInputType, Lang } from '../types';
export interface InputFormProps {
    addTask: (task: FormInputType) => void;
    defaultValues?: FormInputType;
    lang?: Lang;
}
export declare function InputForm({ addTask, defaultValues, lang, }: InputFormProps): JSX.Element;
