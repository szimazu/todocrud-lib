export interface FormInputType {
  task: string
  date: string
  rate: number
  phone: string
}

export interface TaskItemType {
  id: string
  task: string
  date: string
  rate: number
  phone: string
  done: boolean
}
export type Lang = 'en' | 'de'
