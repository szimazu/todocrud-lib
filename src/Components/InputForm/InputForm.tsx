import React from 'react'
import { useForm, Controller } from 'react-hook-form'
import { createIntl, createIntlCache } from 'react-intl'

import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'

import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import Button from 'react-bootstrap/Button'

import { FormInputType, Lang } from '../types'

import { getMesages } from '../utils/getMessages'
export interface InputFormProps {
  addTask: (task: FormInputType) => void
  defaultValues?: FormInputType
  lang?: Lang
}

export function InputForm({
  addTask,
  defaultValues,
  lang = 'en',
}: InputFormProps): JSX.Element {
  const messages = getMesages(lang)
  const cache = createIntlCache()
  const { formatMessage } = createIntl(
    {
      locale: lang,
      messages,
    },
    cache
  )

  const {
    register,
    handleSubmit,
    reset,
    setFocus,
    control,
    formState: { errors },
  } = useForm<FormInputType>({ defaultValues })

  const onSubmit = (data: FormInputType): void => {
    addTask(data)
    reset()
    setFocus('task')
  }

  return (
    <Form className="todo-input--form" onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mb-3">
        <Form.Control
          type="text"
          {...register('task', {
            required: formatMessage({ id: 'inputform.task.error' }),
          })}
          placeholder={formatMessage({ id: 'inputform.task.placeholder' })}
          isInvalid={!!errors.task}
          data-testid="task-input"
        />
        <Form.Control.Feedback type="invalid">
          {errors?.task?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Control
          placeholder={formatMessage({ id: 'inputform.rating.placeholder' })}
          type="number"
          {...register('rate', {
            required: formatMessage({ id: 'inputform.rating.error' }),
            valueAsNumber: true,
            min: {
              value: 1,
              message: formatMessage({ id: 'inputform.rating.error.range' }),
            },
            max: {
              value: 5,
              message: formatMessage({ id: 'inputform.rating.error.range' }),
            },
          })}
          isInvalid={!!errors.rate}
          data-testid="rate-input"
        />
        <Form.Control.Feedback type="invalid">
          {errors?.rate?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group className="mb-3">
        <InputGroup>
          <InputGroup.Text>
            {formatMessage({ id: 'inputform.date.text' })}
          </InputGroup.Text>
          <Form.Control type="date" {...register('date')} />
        </InputGroup>
      </Form.Group>
      <Form.Group className="mb-3">
        <Controller
          control={control}
          name="phone"
          render={({ field: { onChange, value } }) => (
            <PhoneInput value={value} onChange={onChange} />
          )}
        />
      </Form.Group>
      <Button type="submit">{formatMessage({ id: 'btn.add' })}</Button>
    </Form>
  )
}
