import React from 'react'

import { Story, Meta } from '@storybook/react'

import { InputForm } from './InputForm'

export default {
  title: 'Components/InputForm',
  component: InputForm,
} as Meta

const Template: Story = (args: any) => <InputForm {...args} />

export const Primary: Story = Template.bind({})
Primary.args = {
  addTask: (data) => {
    console.log(data)
  },
  defaultValues: { date: '2021-09-22', phone: '', task: '', rate: '' },
}
