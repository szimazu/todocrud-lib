import React from 'react'
import { render, fireEvent, waitFor, screen } from '@testing-library/react'

import renderer from 'react-test-renderer'
import { FormInputType, Lang } from '../types'
import { InputForm } from './InputForm'

describe('Test <InputForm/> Component', () => {
  const defaultValues: FormInputType = {
    task: '',
    rate: 0,
    date: '',
    phone: '',
  }

  const addTaskMockFn = jest.fn()

  const defaultProps = {
    addTask: addTaskMockFn,
    defaultValues,
  }

  beforeEach(() => {
    render(<InputForm {...defaultProps} />)
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  test('should render InputForm in en', () => {
    const tree = renderer.create(<InputForm {...defaultProps} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('should render InputForm in de', () => {
    const lang: Lang = 'de'
    const props = {
      ...defaultProps,
      lang,
    }

    const tree = renderer.create(<InputForm {...props} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('should call addTask function on Form Submit', async () => {
    fireEvent.input(screen.getByTestId('task-input'), {
      target: { value: 'task 1' },
    })
    fireEvent.change(screen.getByTestId('rate-input'), {
      target: { value: '4' },
    })

    await waitFor(() => {
      fireEvent.click(screen.getByText('Add'))
    })

    expect(addTaskMockFn).toHaveBeenCalled()
  })

  test('should show Error message WHEN task input IS EMPTY and rate input OUT OF RANGE', async () => {
    await waitFor(async () => {
      fireEvent.click(screen.getByText('Add'))
    })

    expect(screen.getByText('No task provided!')).toBeInTheDocument()
    expect(screen.getByText('rating value is from 1 - 5')).toBeInTheDocument()
  })
})
