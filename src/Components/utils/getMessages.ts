import { Lang } from '../types'

import German from '../../lang/de.json'
import English from '../../lang/en.json'

export const getMesages = (lang: Lang): Record<string, string> => {
  if (lang === 'de') {
    return German
  }

  return English
}
