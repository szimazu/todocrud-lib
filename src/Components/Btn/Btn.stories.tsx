import React from 'react'

import { Story, Meta } from '@storybook/react'

import { Btn, BtnProps } from './Btn'

export default {
  title: 'Components/Btn',
  component: Btn,
  argTypes: { onClick: { action: 'clicked' } },
} as Meta

const Template: Story<BtnProps> = (args: any) => <Btn {...args} />

export const Primary: Story = Template.bind({})
Primary.args = { children: 'Add' }

export const DeleteBtn: Story = Template.bind({})
DeleteBtn.args = { variant: 'danger', children: 'Delete' }

export const DoneBtn: Story = Template.bind({})
DoneBtn.args = { variant: 'success', children: 'Done' }
