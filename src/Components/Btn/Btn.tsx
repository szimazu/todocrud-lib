import React from 'react'

import Button from 'react-bootstrap/Button'

export interface BtnProps {
  variant?: 'primary' | 'danger' | 'success'
  onClick: () => void
  disabled?: boolean
}

export function Btn({
  variant = 'primary',
  disabled = false,
  onClick,
  children,
}: React.PropsWithChildren<BtnProps>): JSX.Element {
  return (
    <Button
      disabled={disabled}
      variant={variant}
      onClick={onClick}
      onMouseDown={(event) => event.preventDefault()}
    >
      {children}
    </Button>
  )
}
