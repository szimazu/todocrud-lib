import React from 'react'
import renderer from 'react-test-renderer'
import { Btn } from './Btn'

describe('Test <Btn/> Component', () => {
  test('should render Btn', () => {
    const tree = renderer.create(<Btn onClick={jest.fn()}>Add</Btn>).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
