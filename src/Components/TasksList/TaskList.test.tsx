import React from 'react'
import renderer from 'react-test-renderer'

import { TasksList } from './TasksList'
import { Lang } from '../types'

describe('Test <TaskList/> Component', () => {
  const completeTaskMockFn = jest.fn()
  const deteteTaskMockFn = jest.fn()

  const defaultProps = {
    todoItems: [
      {
        id: '1',
        task: 'long text test',
        rate: 5,
        phone: '380111111111',
        date: '02.10.2022',
        done: false,
      },
    ],
    completeTask: completeTaskMockFn,
    deteteTask: deteteTaskMockFn,
  }

  test('should render TaskList in en', () => {
    const tree = renderer.create(<TasksList {...defaultProps} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('should render TaskList in de', () => {
    const lang: Lang = 'de'
    const props = {
      ...defaultProps,
      lang,
    }

    const tree = renderer.create(<TasksList {...props} />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
