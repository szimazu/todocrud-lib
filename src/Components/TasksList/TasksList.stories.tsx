import React from 'react'

import { Story, Meta } from '@storybook/react'

import { TasksList, TasksListProps } from './TasksList'

export default {
  title: 'Components/TasksList',
  component: TasksList,
} as Meta

const Template: Story<TasksListProps> = (args: any) => <TasksList {...args} />

export const List: Story<TasksListProps> = Template.bind({})
List.args = {
  todoItems: [
    {
      id: '1',
      task: 'long text test',
      rate: 5,
      phone: '380111111111',
      date: '02.10.2022',
      done: false,
    },
  ],
}
