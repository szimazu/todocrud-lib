import React from 'react'
import { createIntl, createIntlCache } from 'react-intl'

import ListGroup from 'react-bootstrap/ListGroup'
import { Btn } from '../Btn/Btn'

import { TaskItemType, Lang } from '../types'

import { getMesages } from '../utils/getMessages'

import './TasksList.css'

export interface TasksListProps {
  todoItems: TaskItemType[]
  completeTask: (id: string) => void
  deteteTask: (id: string) => void
  lang?: Lang
}

export function TasksList({
  todoItems,
  completeTask,
  deteteTask,
  lang = 'en',
}: TasksListProps): JSX.Element {
  const messages = getMesages(lang)
  const cache = createIntlCache()
  const { formatMessage } = createIntl(
    {
      locale: lang,
      messages,
    },
    cache
  )
  return (
    <ListGroup>
      {todoItems.map((item) => (
        <ListGroup.Item id="list-item" key={item.id}>
          <div className={`task-text ${item.done ? ' task-text__done' : ''}`}>
            {formatMessage(
              {
                id: 'tasklist.list.item',
              },
              {
                task: item.task,
                date: item.date,
                rate: item.rate,
                phone: item.phone,
              }
            )}
          </div>
          <Btn
            disabled={item.done}
            variant={'success'}
            onClick={() => completeTask(item.id)}
          >
            {formatMessage({ id: 'btn.done' })}
          </Btn>
          <Btn variant={'danger'} onClick={() => deteteTask(item.id)}>
            {formatMessage({ id: 'btn.delete' })}
          </Btn>
        </ListGroup.Item>
      ))}
    </ListGroup>
  )
}
